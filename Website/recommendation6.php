<? 
$page = 'goal2';
$pageTitle = 'Training: Recommendation 6';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="green clearfix">
      
      <div id="content" class="green">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Establish a national intervener jobs clearinghouse to assist in intervener recruitment and job placement.</div>
              <div class="number green">6</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">Currently, the number of interveners working in the U.S. is small and 
                  there is no system in place to track who they are or where they work. As the use of intervener 
                  services grows, it will be important to have a centralized way to help interveners find 
                  employment, to assist school districts in recruiting qualified staff, and to track the number 
                  of trained interveners available nationwide.</div>
              <a href="#" class="more green expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix strategies">
                  <p>
                      Click "Read More" to see the 4 implementation strategies for this recommendation.
                  </p>
                  <p class="bullet">Convene a workgroup of interveners and other individuals who have knowledge of intervener hiring practices (e.g., educational administrators, state deaf-blind project personnel) to determine the elements needed to design an online jobs clearinghouse.</p>
                  <p class="bullet">Develop a secure online jobs clearinghouse reflecting those elements.</p>
                  <p class="bullet">Publicize the availability of the clearinghouse through current intervener training programs, interpreter training programs, state deaf-blind projects, and other relevant agencies and organizations.</p>
                  <p class="bullet">Maintain the clearinghouse data on an ongoing basis.</p>
              </div>
              <a href="#" class="more green expando strategies">Read more</a>
          </div>
          
          <div class="item clearfix last">
              <h2>Anticipated Outcomes</h2>
              <div class="content">
                  <ul>
                      <li>Improved intervener recruitment.</li>
                      <li>Intervener access to the clearinghouse for the purpose of identifying employment opportunities.</li>
                  </ul>                  
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="green">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec6.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="recommendation5.php" class="navButton green left"><span>Recommendation 5</span></a>
      <a href="recommendation7.php" class="navButton green right"><span>Recommendation 7&nbsp;</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>