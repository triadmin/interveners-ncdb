<? 
$page = 'goal2';
$pageTitle = 'Training: Recommendation 7';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="green clearfix">
      <div id="content" class="green">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Provide resources (e.g., technology applications, technical assistance) that 
                  help interveners establish organized online and face-to-face communities where they can 
                  improve their knowledge and skills by sharing ideas and experiences with each other.</div>
              <div class="number green">7</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">Because deaf-blindness is a very low-incidence disability, children 
                  who are deaf-blind are typically widely dispersed.  As a result, interveners 
                  often report feeling isolated and lack opportunities to interact with and learn from each 
                  other.  Some state deaf-blind projects address this within individual states by creating 
                  online discussion groups or occasional face-to-face meetings, but this is not available in 
                  most states and there are currently no organized state or national communities of interveners.</div>
              <a href="#" class="more green expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix strategies">
                  <p>
                      Click "Read More" to see the 4 implementation strategies for this recommendation.
                  </p>
                  <p class="bullet">Convene a workgroup of interveners, state deaf-blind project personnel, and university faculty to determine desired features of an online community of interveners.</p>
                  <p class="bullet">Develop and maintain a web-based platform providing those features.</p>
                  <p class="bullet">Publicize the availability of the site and train interveners in its use.</p>
                  <p class="bullet">Explore opportunities for interveners to occasionally meet in person (e.g., state meetings, national or regional conferences).</p>
                  
              </div>
              <a href="#" class="more green expando strategies">Read more</a>
          </div>
          
          <div class="item clearfix last">
              <h2>Anticipated Outcomes</h2>
              <div class="content">
                  <ul>
                      <li>Better prepared, more knowledgeable interveners.</li>
                      <li>A decrease in the isolation experienced by interveners.</li>
                      <li>Active state and national intervener communities.</li>
                  </ul>
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="green">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec7.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
          <div class="panel">
              <a href="media/charts/Recommendation_7a.png" title="" class="lightbox">
              <img src="images/chartThumb.png" title="Data charts" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="recommendation6.php" class="navButton green left"><span>Recommendation 6</span></a>
      <a href="broadGoalsFamilies.php" class="navButton maroon right"><span>Families&nbsp;</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>