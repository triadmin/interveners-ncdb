<? 
$page = 'goal1';
$pageTitle = 'Recognition: Recommendation 1';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="blue clearfix">
      <div id="content" class="blue">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Develop a coordinated and expanded national approach to 
                  provide state and local early intervention and education agencies with 
                  information and tools needed to understand and use intervener services.</div>
              <div class="number blue">1</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">Many passionate leaders have worked for decades to expand intervener services and make 
                  them as effective as possible. Yet the practice remains inconsistently implemented, misunderstood, and 
                  relatively unknown in some states. In addition, there is significant variation in how the term "intervener" 
                  is used, especially with respect to the type of training that should be required for interveners. In some 
                  circumstances, the designation of “intervener” is inaccurately used to describe a paraeducator who works 
                  one-on-one with a child who is deaf-blind but has not had training in deaf-blindness. What is needed now 
                  is a consistently applied definition of intervener services and a nationally coordinated effort among 
                  multiple stakeholders that brings together knowledge and innovative strategies that are currently dispersed 
                  unevenly across the country, in order to build a comprehensive foundation for intervener services.</div>
              <a href="#" class="more blue expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed strategies clearfix">
                  <p>
                      Click "Read More" to see the 3 implementation strategies for this recommendation.
                  </p>
                  <p class="bullet" style="margin-top: 30px;">The National Consortium on Deaf-Blindness (NCDB), state deaf-blind projects, 
                      and other stakeholders (e.g., families, early interventionists, teachers, related 
                      service providers, early intervention and educational administrators, interveners, 
                      and university faculty), will join forces to implement a comprehensive national 
                      intervener initiative.  The initiative, coordinated by NCDB, will:</p>
                  <ul class="ulBumpLeft">
                      <li>develop and disseminate a consistently applied national definition of intervener services, including clarification of the occupational role of the intervener;</li>
                      <li>organize workgroups to implement the recommendations in Goals 1 through 3 and identify additional needs and recommendations to improve intervener services, including at a minimum:
                          <ul>
                              <li>intervener preparation and training,</li>
                              <li>continuing education needs of interveners,</li>
                              <li>coaching and supervision of interveners,</li>
                              <li>credentialing or certification of interveners,</li>
                              <li>interveners in community and home settings, and</li>
                              <li>interveners for infants and toddlers.</li>
                          </ul>
                      </li>
                      <li>create a web-based platform on which state deaf-blind projects, NCDB, families, and other organizations and individuals can interact and share knowledge—for example, to:
                          <ul>
                              <li>communicate ideas and concerns,</li>
                              <li>highlight intervener training and support models, and</li>
                              <li>access a shared video library related to intervener services (e.g., parent and professional insights, examples of interveners working with children).</li>
                          </ul>
                      </li>
                      <li>identify and implement strategies to increase collaborative efforts between agencies and organizations within individual states (e.g., state deaf-blind projects, parent training and information centers, family organizations) to improve intervener services at the state level.</li>
                  </ul>
                  
                  <p class="bullet">Develop and make available a core set of publications that increase 
                      understanding of intervener services and promote their development and use including, at a minimum:</p>
                  <ul class="ulBumpLeft">
                      <li>concise fact sheets that promote an enhanced understanding of intervener services 
                          and explain the occupational role of a well-trained intervener,</li>
                      <li>publications that highlight promising intervener-training and support 
                          programs and provide strategies that describe how they can be 
                          replicated, and</li>
                      <li>publications that describe effective practices for intervener services.</li>
                  </ul>
                  
                  <p class="bullet">Design and launch a national data collection program to collect, 
                      compile, and make available data about the use of intervener services including:</p>
                  <ul class="ulBumpLeft">
                      <li>characteristics of interveners and patterns of use (e.g., how many, where employed, education level),</li>
                      <li>characteristics of children and youth who receive intervener services, and</li>
                      <li>the nature of services being provided by interveners.</li>
                  </ul>
              </div>
              <a href="#" class="more blue strategies expando">Read more</a>
          </div>
          
          <div class="item clearfix last">
              <h2>Anticipated Outcomes</h2>
              <div class="content">
                  <ul>
                      <li>An organized, cohesive, systematic approach to promoting and improving intervener services in the U.S.</li>
                      <li>A national definition of intervener services that includes a clear description of the occupational role of an intervener.</li>
                      <li>Improved recognition and appropriate use of intervener services.</li>
                  </ul> 
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="blue">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec1.png" title="Community Voices" class="lightbox">
                  <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
          <div class="panel">
              <a href="media/charts/Recommendation_1a.png" title="" class="lightbox">
              <img src="images/chartThumb.png" title="Percent of state deaf-blind projects that collaborate with other agencies/organizations to improve intervener services" />
              </a>
              <a href="media/charts/Recommendation_1b.png" title="" class="lightbox hide"></a>
          </div>
          <div class="panel">
              <a href="http://c324175.r75.cf1.rackcdn.com/products/definition.pdf" title="Data Summary" target="_blank">
              <img src="images/dataSummaryThumb.png" title="Data Summary" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="recommendation2.php" class="navButton blue right"><span>Recommendation 2</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>