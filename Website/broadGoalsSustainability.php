<? 
$page = 'goal4';
$pageTitle = 'Sustainability: Recommendations';
include('includes/header.php') ;?>
    <div role="main" id="broadGoals" class="clearfix">
      <h1>Goal 4</h1>
      <p class="header orange">Sustain high-quality intervener services across the nation through the inclusion of intervener services in national special education policy.</p>
      <div class="clearfix"></div>
      
      <div id="content" class="orange">
        <div class="subheader closed">
            <h2>Discussion</h2>
            <div class="content">
            <p>The intent of the recommendations outlined in Goals 1, 2, and 3 is to build a solid foundation 
                for the provision of intervener services throughout the United States.  Many individuals and 
                agencies across the country have a passionate interest in ensuring the availability of 
                high-quality intervener services for children and youth who are deaf-blind.  They include 
                parents, state deaf-blind project personnel, teachers, administrators, interveners, university 
                faculty, NCDB, and others.  While it is true that implementation will take time and hard work, 
                these recommendations can be accomplished with a united effort by this broad community.</p>
            
            <p>However, sustaining progress gained through the achievement of recommendations outlined in this 
                report is likely to be extremely difficult without legislation that recognizes intervener 
                services as related service and early intervention service options. Therefore, the single 
                recommendation for this final goal is to include such a provision in the Individuals with 
                Disabilities Education Act (IDEA).</p>
            
            <p>Children and youth who are deaf-blind are a diverse, low-incidence, 
                geographically dispersed population of students.  It is often difficult to gain 
                attention to their need for specialized services in today's complex special 
                education systems.  For decades, though, services for children with this disability 
                have been mandated in federal education law.  IDEA specifies a minimum funding 
                level to “address the educational, related services, transitional, and early 
                intervention needs of children with deaf-blindness” (IDEA, 2004a).  The current 
                deaf-blind technical assistance network, consisting of one national center (NCDB) 
                and 52 state deaf-blind projects is funded under this mandate to build the 
                capacity of early intervention and education agencies in every state to serve 
                students with deaf-blindness.  While current language in IDEA does not preclude the 
                use of intervener services for children who are deaf-blind, including these services 
                in IDEA as both a related service and an early intervention service would highlight 
                the need for these services for many children. The combination of technical assistance 
                provided by the state deaf-blind projects and improved provision of intervener services 
                at the local level would enhance the capacity of states to provide children who are 
                deaf-blind with a free appropriate public education in the least restrictive environment.</p>
            
            <p>The decision to recommend advocating for the inclusion of intervener services in IDEA was 
                not made lightly. The implications would be significant at national, state, and local 
                levels and it is not coincidental that this recommendation is the last. While inclusion 
                of intervener services in IDEA would enhance the long-term sustainability of these 
                services in our country, doing so before a foundation is established could actually 
                have a negative impact on the long-term provision and availability of high-quality 
                intervener services. If systems are not in place to recruit, train, and provide on-the-job 
                support and supervision for interveners and to assist IFSP/IEP teams in determining a 
                child’s need for intervener services, schools and other agencies responsible for FAPE 
                would struggle to meet the demand.</p>
            
            <p>A number of features of the other recommendations in this report must be in 
                place in order to be ready for the increase in the use of intervener services 
                that inclusion in IDEA could create.  At a minimum, significant progress will 
                need to be made on the following:
            <ul>
                <li>increasing the educational systems’ understanding of the purpose of intervener services and their potential benefits for children and youth with deaf-blindness (Recommendation 1);</li>
                <li>ensuring that interveners have access to high-quality training (Recommendation 3);</li>
                <li>ensuring that interveners have ready access to workplace supports at the local level, including professionals with expertise in deaf-blindness (Recommendation 4); and</li>
                <li>enhancing family involvement in decision-making about intervener services for their children (Recommendation 8).</li>
            </ul>
            </p>
            
            <p>These foundational efforts will help to ensure that a base of support for intervener 
                services is well established, thus maximizing the likelihood that congressional 
                decision-makers will understand both the importance and the implications of 
                the inclusion of a provision about intervener services in IDEA.</p>
            </div>
          
        </div>
        <a href="#" class="more orange expando">Read more</a>   
        <div class="clearfix"></div>
        
        <div id="broadGoalsContainer">
            <!-- BEGIN :: Broad Goal Panel -->
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 10</div>
                    <div class="photo orange"></div>
                    <div class="text orange">Congress should ensure the long-term sustainability of 
                        intervener services for children and youth who are deaf-blind by including 
                        them  under the definition of "related service" and as an early intervention 
                        service in the next reauthorization of the Individuals with Disabilities 
                        Education Act (IDEA).</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation10.php" class="button orange">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END :: Broad Goal Panel -->
            
            <div class="clearfix"></div>
        </div>     
        
        <div class="clearfix"></div>
      </div>
  </div>
  <? include('includes/footer.php') ;?>