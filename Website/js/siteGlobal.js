var site = {
    init : function() {
        $('.expando').click(function() {
           var el = $(this);
           if (el.hasClass('more')) {
               el.removeClass('more').addClass('less');
               el.prev().removeClass('closed').addClass('open');
               el.html('Close');
           } else {
               el.removeClass('less').addClass('more');
               el.prev().removeClass('open').addClass('closed');
               el.html('Read More');
           }
           return false;
        });
        
        $('.lightbox').colorbox({
            rel: 'lightbox', 
            transition: 'fade',
            height: '90%',
            scalePhotos: false,
            current: ''
        });
        
        $('.homeVideo').colorbox({
            iframe: true,
            innerWidth: 700,
            innerHeight: 431
        });
        
        $('.flexslider').flexslider({
            animation: 'slide',
            slideshowSpeed: 7000,
            animationDuration: 300,
            directionNav: false
        });
    }
}

$(document).ready(function() {
   site.init(); 
});