<? 
$page = 'goal2';
$pageTitle = 'Training: Recommendation 5';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="green clearfix">
      
      <div id="content" class="green">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Expand opportunities for interveners to obtain a state or national certificate or credential.</div>
              <div class="number green">5</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">Support for intervener certification or credentialing (hereafter 
                  referred to only as “credentialing” for simplicity) is strong in the field of deaf-blindness. 
                  A new national credential for interveners became available in 2011 as the result of the 
                  efforts of a group of individuals associated with the National Intervener Task Force. 
                  However, while the national credential—offered by National Resource Center on Paraeducators 
                  (NRCP)—is a positive step forward, it requires 10 credits of intervener coursework at a 
                  college or university, which is currently available at one of only two existing university 
                  programs. It does not allow for pursuit of a credential by interveners who attend other 
                  programs, such as those sponsored by state deaf-blind projects or based on continuing 
                  education units instead of credit hours. A requirement that credentialing criteria include 
                  credited coursework may be feasible in the future, but at the present time, it should be broadened to include 
                  other options.</div>
              <a href="#" class="more green expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix strategies">
                  <p>
                      Click "Read More" to see the 1 implementation strategy for this recommendation.
                  </p>
                  <p class="bullet">In partnership with stakeholders, including the NRCP and the National Intervener Task Force, and with input from a broad group of stake holders:</p>
                  <ul class="ulBumpLeft">
                      <li>review the current National Intervener Credential, including the criteria and processes involved;</li>
                      <li>consider current and future needs for an intervener credential and short- and long-term goals of intervener credentialing; </li>
                      <li>determine additional needs that may exist related to a national intervener credential;</li>
                      <li>if needed, identify additional credentialing bodies that could offer a national credential that meets those needs; and</li>
                      <li>determine the most feasible credentialing options and move forward with efforts to expand pathways to a national credential that are applicable to interveners with a variety of training backgrounds.</li>
                  </ul>
                  <!--<p class="bullet">In partnership with stakeholders, including the NRCP and the National Intervener Task Force, determine the necessary criteria for an intervener credential.</p>
                  <p class="bullet">Identify credentialing bodies that could offer a national credential that meets those criteria.</p>
                  <p class="bullet">Determine the most feasible credentialing options and move forward with efforts to expand pathways to a national credential that are applicable to interveners with a variety of training backgrounds. </p>-->

              </div>
              <a href="#" class="more green expando strategies">Read more</a>
          </div>
          
          <div class="item clearfix last">
              <h2>Anticipated Outcomes</h2>
              <div class="content">
                  <ul>
                      <li>Increased opportunities for interveners to gain a credential indicating they have acquired the core set of skills needed to provide intervener services.</li>
                      <li>A consistent way for early intervention and education agencies to identify interveners who have acquired the core set of skills needed to provide intervener services.</li>
                  </ul>    
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="green">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec5.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
          <div class="panel">
              <a href="http://www.nationaldb.org/documents/products/credentialing.pdf" title="Data Summary" target="_blank">
              <img src="images/dataSummaryThumb.png" title="Data Summary" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="recommendation4.php" class="navButton green left"><span>Recommendation 4</span></a>
      <a href="recommendation6.php" class="navButton green right"><span>Recommendation 6&nbsp;</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>