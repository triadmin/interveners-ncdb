<? 
$page = 'goal3';
$pageTitle = 'Families: Recommendations';
include('includes/header.php') ;?>
    <div role="main" id="broadGoals" class="clearfix">
      <h1>Goal 3</h1>
      <p class="header maroon">Build the capacity of families to participate in decisions about intervener services for their children and in efforts to improve these services.</p>
      <div class="clearfix"></div>
      
      <div id="content" class="maroon">
        <div class="subheader closed">
            <h2>Discussion</h2>
            <div class="content">
            <p>For many years, family advocacy has been a driving force behind the movement to 
                make intervener services a recognized and viable option for children who are 
                deaf-blind.  It is not surprising, therefore, that there was great interest and 
                participation from families in NCDB’s initiative to develop recommendations for 
                improving intervener services.  One hundred nineteen individuals completed a 
                survey for parents and guardians, and thirteen represented the parent perspective 
                on a family panel.  In survey comments and during the panel discussion, they 
                described the challenges and successes experienced when advocating for intervener 
                services for their children.  Some described the current system as one that 
                places them in an adversarial position with educators and school districts and 
                shared their frustration at being put in such a position.  Parents whose children 
                have interveners reported that their own advocacy was a key factor that led to 
                their child's receiving those services.  Many indicated that having an intervener 
                has had a profoundly positive impact on their child’s learning and quality of life.</p>
            
            <p>Collectively, the purpose of all of the recommendations in this report is to create 
                a national system for intervener services centered on what is best for children 
                and families.  Family input and participation will be essential when each 
                recommendation is carried out.  However, families also have specific needs that 
                are not addressed by the recommendations for Goals 1 and 2.  Therefore, the two recommendations 
                for Goal 3 supplement the others by creating resources and tools for 
                families.
            </p>
            </div>
        </div>
        <a href="#" class="more maroon expando">Read more</a>   
        <div class="clearfix"></div>
        
        <div id="broadGoalsContainer">
            <!-- BEGIN :: Broad Goal Panel -->
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 8</div>
                    <div class="photo maroon"></div>
                    <div class="text maroon">Develop information resources and tools and disseminate them to 
                        family members to increase their knowledge of intervener services and enhance their 
                        ability to communicate effectively with educators, administrators, and others about 
                        those services.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation8.php" class="button maroon">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END :: Broad Goal Panel -->
            
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 9</div>
                    <div class="photo maroon"></div>
                    <div class="text maroon">Develop and implement strategies that create 
                        opportunities for families to share ideas and experiences and work 
                        together to address intervener services at local, state, and national 
                        levels.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation9.php" class="button maroon">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>     
        <div class="clearfix"></div>
        
      </div>
  </div>
  <? include('includes/footer.php') ;?>