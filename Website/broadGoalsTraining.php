<? 
$page = 'goal2';
$pageTitle = 'Training: Recommendations';
include('includes/header.php') ;?>
    <div role="main" id="broadGoals" class="clearfix">
      <h1>Goal 2</h1>
      <p class="header green">Establish a strong national foundation for intervener training and workplace supports.</p>
      <div class="clearfix"></div>
      
      <div id="content" class="green">
        <div class="subheader closed">
            <h2>Discussion</h2>
            <div class="content">
                <p>NCDB designed the recommendations for Goal 1 to expand national, state, and local recognition and use of effective intervener services. They focus on efforts to increase knowledge of those services beyond the field of deaf-blindness. The recommendations for Goal 2 turn the focus back to the field by emphasizing the need to strengthen the current system of preparing and training interveners as well as the workplace supports available for interveners. These are intended to ensure that a) a sufficient number of well-trained interveners are available for children who require intervener services and b) working interveners have knowledgeable supervisors and access to experts in deaf-blindness.</p>
                
                <p>Without an adequate supply of qualified interveners and an understanding of their role, decisions about the need for intervener services are more likely to be driven by the availability of an intervener rather than by a child’s needs. That compromises the requirements of IDEA to develop and implement an individualized program of instruction to meet a child's unique needs as identified through appropriate evaluation. And when interveners are available, the service will not be effective without support from supervisors and expert consultants who can help interveners build their skills and respond to the changing needs of the children with whom they work.</p>
                
                <p>Currently, specialized training to prepare interveners to work with children and youth who are deaf-blind is available through distance-education programs at two universities (East Carolina University and Utah State University) and, in several states, through programs operated by state deaf-blind projects. In addition, approximately 20 state projects report providing some support to interveners who are enrolled in one of the university programs, including tuition stipends, on-the-job coaching, and annual face-to-face workshops.</p>
                
                <p>The task of providing workplace supports to interveners typically falls on state deaf-blind projects.  A number of the projects have implemented strategies to provide training about intervener services to teachers and other team members and improve interveners' access to experts in deaf-blind education.  Although the state deaf-blind projects have accomplished a great deal, limited staffing, restricted budgets, and the demands of meeting additional family and child objectives place serious constraints on a project’s capacity to fully implement a training and support system for interveners.  In addition, some projects report that their state and local education agencies do not support intervener services or specialized intervener training.  
                    Based on NCDB’s review of current intervener services in the U.S., all of these factors contribute to a small number of qualified interveners in most states or none at all.</p>

                <p>Significant efforts have gone into the development and updating of high-quality 
                    intervener training programs at East Carolina University and Utah State University. 
                    In addition, both are designed to enable their students to meet the Council for 
                    Exceptional Children’s <em>Specialization Knowledge and Skill Set for Paraeducators 
                    Who Are Interveners for Individuals with Deaf-blindness</em>. While the efforts of 
                    these programs and the state deaf-blind projects serve as a solid foundation 
                    upon which future training and workplace support systems can be built, it is 
                    clear that the existing national infrastructure is not adequate to meet current 
                    and anticipated future demands for intervener services. In the United States, 
                    almost 10,000 children between the ages of birth and 22 have been identified as 
                    being deaf-blind (NCDB, 2010). An NCDB survey of state deaf-blind projects 
                    suggests that fewer than 5% receive intervener services. It is not known for 
                    certain how many children require intervener services to receive a free and 
                    appropriate public education (FAPE); however, given the profound limitations 
                    that combined vision and hearing loss place on a child’s ability to access 
                    information and communicate, it is likely that many more children would benefit 
                    from these services.</p>
                
                <p>As recognition of intervener services increases, their use is likely to expand dramatically.  It is crucial that the field of deaf-blindness prepare for this increase by strengthening the current system of intervener training and workplace supports.</p>

                <p>Moving forward, there will also be a need for interveners to demonstrate that 
                    they have met basic competency standards by obtaining a national or state 
                    intervener certificate or credential.  NCDB survey results and panel discussions showed there
                    is widespread support within the field of deaf-blindness for national credentialing of interveners, but there are unresolved issues about criteria and testing requirements and concerns about unintended consequences should certification or credentialing become mandatory.</p>

                <p>The purposes of the recommendations for this goal are to provide resources and national coordination to expand current activities related to intervener training and continuing education, intervener access to experts in deaf-blindness, and opportunities for credentialing or certification. They also respond to the challenge of recruiting interveners and the need for mechanisms that allow interveners to form communities in which they can learn from each other.</p>
            
                <h3>References</h3>
                <p>Council for Exceptional Children. (2009). Specialization knowledge and skill set for 
                    paraeducators who are interveners for individuals with deaf-blindness. <em>What every 
                    special educator must know: Ethics, standards, and guidelines</em>, 6th ed., pp. 195-201. 
                    Arlington, VA: CEC.</p>
                <p>NCDB. (2011.) <em>The 2010 national child count of children and youth who are deaf-blind</em>. 
                    Retrieved from: <a href="http://www.nationaldb.org/documents/products/2010-Census-Tables.pdf">http://www.nationaldb.org/documents/products/2010-Census-Tables.pdf</a></p>
            </div>
        </div>
        <a href="#" class="more green expando">Read more</a>   
        <div class="clearfix"></div>
        
        <div id="broadGoalsContainer">
            <!-- BEGIN :: Broad Goal Panel -->
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 3</div>
                    <div class="photo green"></div>
                    <div class="text green">Develop a national open-access intervener-training 
                        curriculum that aligns with the Council for Exceptional Children’s 
                        <em>Specialization Knowledge and Skill Set for Paraeducators Who Are Interveners 
                        for Individuals with Deaf-blindness</em>.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation3.php" class="button green">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END :: Broad Goal Panel -->
            
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 4</div>
                    <div class="photo green"></div>
                    <div class="text green">Develop strategies to ensure that interveners have knowledgeable supervisors and access to 
                        experts in deaf-blindness who can provide consultation and coaching.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation4.php" class="button green">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 5</div>
                    <div class="photo green"></div>
                    <div class="text green">Expand opportunities for interveners to obtain a state or national certificate or credential.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation5.php" class="button green">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 6</div>
                    <div class="photo green"></div>
                    <div class="text green">Establish a national intervener jobs clearinghouse to assist in intervener recruitment and job placement.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation6.php" class="button green">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 7</div>
                    <div class="photo green"></div>
                    <div class="text green">Provide resources (e.g., technology applications, technical 
                        assistance) that help interveners establish organized online and face-to-face 
                        communities where they can improve their knowledge and skills by sharing ideas and 
                        experiences with each other.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation7.php" class="button green">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>     
        <div class="clearfix"></div>
        
      </div>
  </div>
  <? include('includes/footer.php') ;?>