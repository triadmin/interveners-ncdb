<? 
$pageTitle = 'Recommendations Development Process';
include('includes/header.php'); ?>
<div role="main" class="clearfix" id="broadGoals">
    <h1 style="width: 100%;">Recommendations Development Process</h1>
    <div class="clearfix"></div>
    <div id="content" class="plain">  
        <p class="mBottom24">NCDB developed these recommendations in response to a request from the Department of Education’s Office of Special
        Education Programs (OSEP) in October 2011. As noted throughout this report, the
        recommendations are grounded upon the achievements of many individuals who have worked
        diligently for years to promote intervener services in the U.S.</p>

        <p>The process of developing the recommendations involved two phases:</p>
        <ol type="numeric" class="mBottom24">
            <li>The collection of detailed information about the current status of intervener services and
            practices in the U.S.</li>
            <li>Development of recommendations to improve future intervener services based on an
            analysis of the information gathered during Phase 1.</li>
        </ol>    

        <h2>Phase 1: Information Gathering</h2>
        <p class="mBottom24">NCDB used the following combination of methods to gather data and other 
            information from a variety of sources. This is the first time that comprehensive information 
            about intervener services in the U.S. has been collected.</p>

        <h3>Surveys</h3>
        <p>In-depth surveys were developed to gather specific information from four key groups of
        individuals with knowledge of intervener services:</p>
        <ul class="mBottom24">
            <li>state deaf-blind projects,</li>
            <li>parents and guardians of children who are deaf-blind,</li>
            <li>interveners, and</li>
            <li>early intervention and educational administrators.</li>
        </ul>
        
        <p class="mBottom24">All of the surveys were anonymous and administered online. To gain as large a 
            response as possible, we announced the surveys widely across the network of deaf-blind projects 
            and organizations using a variety of e-mail discussion groups, web sites, and Facebook pages.</p>

        <h3>Interviews</h3>
        <p>To learn specific details about current methods of intervener training, we conducted extensive
        interviews with:</p>
        <ul class="mBottom24">
            <li>the directors of the two online university intervener-training programs &mdash; 
                East Carolina University and Utah State University &mdash; and</li>
            <li>representatives from 25 state deaf-blind projects.</li>
        </ul>    

        <h3>Visits to two state deaf-blind projects</h3>
        <p class="mBottom24">NCDB staff visited the Minnesota and Texas state deaf-blind projects to learn about methods of
        intervener training and workplace supports and discuss issues regarding the use of interveners in
        their states. Both projects have long histories of active involvement in intervener services.</p>

        <h3>Literature reviews and document collection</h3>
        <p class="mBottom24">We conducted a detailed review of the literature
        on intervener services. Prior to this initiative, NCDB already had a comprehensive collection
        of information resources as part of its DB-LINK Library. We also identified and collected a
        number of unpublished documents from individuals who participated in interviews.</p>

        <p class="mBottom24">In addition to formal information-gathering strategies, NCDB made announcements 
            about the initiative to the field and invited anyone to contact us with questions or comments. 
            The resulting informal conversations informed the work of the initiative.</p>

        <p class="mBottom24">A great deal of information was collected during Phase 1 and data summaries 
            describing some of this information are provided throughout this report. A more detailed 
            report of the findings is under development.</p>

        <h2>Phase 2: Development of Recommendations</h2>
        <p class="mBottom24">In late March and early April of 2012, prior to developing the recommendations, NCDB
        facilitated six 2-hour online discussion panels consisting of groups of individuals with a strong
        interest in intervener services. There were two panels of combined state deaf-blind project
        personnel and university faculty, and one panel each of parents or guardians, interveners,
        teachers, and early intervention or educational administrators.</p>

        <p class="mBottom24">Prior to panels, NCDB provided the participants with summaries of the information 
            gathered during Phase 1, and during the panels, NCDB asked them to respond to structured questions. 
            The questions were based on an analysis by the NCDB Intervener Initiative Team of data gathered 
            during Phase 1. Each panel was facilitated. Participants also had an opportunity to provide any 
            additional information they wished to share either in writing or by phone following their panel.</p>

        <p class="mBottom24">After all of the panels were completed, NCDB staff conducted numerous meetings to develop the
        recommendations. They are based primarily on our analysis and interpretation of information
        collected during Phase 1 and insights gained during the panel discussions.</p>
    </div>
</div>    
<? include('includes/footer.php'); ?>
