<? 
$page = 'goal10';
$pageTitle = 'Sustainability: Recommendation 10';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="orange clearfix">
      <div id="content" class="orange">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Congress should ensure the long-term sustainability of 
                intervener services for children and youth who are deaf-blind by including 
                them  under the definition of "related service" and as an early intervention 
                service in the next reauthorization of the Individuals with Disabilities 
                Education Act (IDEA).</div>
              <div class="number orange">10</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">For many children and youth who are deaf-blind, access 
                  to high quality intervener services is needed to support specially designed 
                  instruction identified in an IFSP or IEP.  By definition, a related service 
                  is a service that “may be required to assist a child with a disability to 
                  benefit from special education” (IDEA, 2004b).  Early intervention services 
                  are defined, in part, as services that “are designed to meet the developmental 
                  needs of an infant or toddler with a disability” (IDEA, 2004c).  Clearly, 
                  many children who are deaf-blind need intervener services in order to benefit 
                  from the special education provided to them.  Quality intervener services 
                  support a child’s ability to participate appropriately in developmental and 
                  educational opportunities, to engage with the physical environment, and to 
                  access the general curriculum.  Children who are deaf-blind would be well 
                  served if IDEA specifically identified intervener services as both a related service 
                  and an early intervention service option.</div>
              <a href="#" class="more orange expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>References</h2>
              <div class="content clearfix">
                  <p>Individuals with Disabilities Education Act, 20 U. S. C. § 1482 (2004a).</p>
                  <p>Individuals with Disabilities Education Act, 20 U. S. C. § 1401 (2004b).</p>
                  <p>Individuals with Disabilities Education Act, 20 U. S. C. § 1432 (2004c).</p>
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="orange">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec10.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="recommendation9.php" class="navButton maroon left"><span>Recommendation 9</span></a>
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>