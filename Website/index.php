<? 
$page = 'home';
include('includes/header.php'); ?>
  <div role="main" class="clearfix">
      <h1 class="tagline">Recommendations for Improving Intervener Services</h1>      
    <div id="sliderContainer" class="flexslider">
        <ul class="slides">
            <li>
                <div class="photo homeVideoStill2">
                    <!-- 43203256 -->
                    <a href="http://player.vimeo.com/video/43857452" class="homeVideo"></a>
                </div>
                <div class="text">
                    <h2>Introduction and Welcome</h2>
                    <p>Jay Gense</p>
                </div>
            </li>
            <li>
                <div class="photo homeVideoStill1">
                    <a href="http://player.vimeo.com/video/43065680" class="homeVideo"></a>
                </div>
                <div class="text">
                    <h2>Eduardo Madera on the Function of the Intervener</h2>
                    <p></p>
                </div>
            </li>
            <li>
                <div class="photo homeVideoStill3">
                    <a href="http://player.vimeo.com/video/43217796" class="homeVideo"></a>
                </div>
                <div class="text">
                    <h2>Interveners at Work</h2>
                    <p></p>
                </div>
            </li>
        </ul> 
    </div> 
    <div id="divider" class="mTop12">
        <h2><a href="welcome.php">Read the "Overview and Welcome"</a></h2>
    </div>
      <div id="broadGoalsContainer">
          <div class="broadGoalsPanel spacer">
              <div class="clipTop blue">
                  <a href="broadGoalsRecognition.php" title="Goal 1: Recognition">Goal<br />
                      <span class="number">1</span></a>
              </div>
              <div class="header">Recognition</div>
              <div class="photo blue"></div>
              <div class="text blue">Increase recognition and appropriate use of intervener 
                  services for children and youth who are deaf-blind.</div>
              <div class="footer">
                  <div class="buttonContainer">
                      <a href="broadGoalsRecognition.php" class="button blue">Recommendations</a>
                  </div>
              </div>
          </div>
          <div class="broadGoalsPanel spacer">
              <div class="clipTop green">
                  <a href="broadGoalsTraining.php" title="Goal 2: Training">Goal<br />
                      <span class="number">2</span></a>
              </div>
              <div class="header">Training &amp; Support</div>
              <div class="photo green"></div>
              <div class="text green">Establish a strong national foundation for intervener training and 
                  workplace supports.</div>
              <div class="footer">
                  <div class="buttonContainer">
                      <a href="broadGoalsTraining.php" class="button green">Recommendations</a>
                  </div>
              </div>
          </div>
          <div class="broadGoalsPanel spacer">
              <div class="clipTop maroon">
                  <a href="broadGoalsFamilies.php" title="Goal 3: Families">Goal<br />
                      <span class="number">3</span></a>
              </div>
              <div class="header">Families</div>
              <div class="photo maroon"></div>
              <div class="text maroon">Build the capacity of families to participate in decisions about intervener services for their children and in efforts to improve these services.</div>
              <div class="footer">
                  <div class="buttonContainer">
                      <a href="broadGoalsFamilies.php" class="button maroon">Recommendations</a>
                  </div>
              </div>
          </div>
          <div class="broadGoalsPanel">
              <div class="clipTop orange">
                  <a href="broadGoalsSustainability.php" title="Goal 4: Sustainability">Goal<br />
                      <span class="number">4</span></a>
              </div>
              <div class="header">Sustainability</div>
              <div class="photo orange"></div>
              <div class="text orange">Sustain high-quality intervener services across the nation through the inclusion of intervener services in national special education policy.</div>
              <div class="footer">
                  <div class="buttonContainer">
                      <a href="broadGoalsSustainability.php" class="button orange">Recommendations</a>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <? include('includes/footer.php'); ?>