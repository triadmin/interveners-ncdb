<? 
$pageTitle = 'About NCDB';
include('includes/header.php'); ?>
<div role="main" class="clearfix" id="broadGoals">
    <h1 style="width: 100%;">About the National Consortium on Deaf-Blindness (NCDB)</h1>
    <div class="clearfix"></div>
    <div id="content" class="plain">  

        <p><a href="http://www.nationaldb.org" title="NCDB Home">NCDB</a> is a national technical assistance (TA) and dissemination center for children and youth
        who are deaf-blind. It is funded by the U.S. Department of Education's Office of Special
        Education Programs (OSEP). In partnership with state deaf-blind TA projects throughout the
        U.S., NCDB conducts initiatives and activities to achieve the following goals:</p>

        <ul class="mBottom24">
            <li>increase the capacity of state and local early intervention and education agencies to
            improve policies and practices for children and youth who are deaf-blind;</li>
            <li>promote the use of evidence-based practices; and</li>
            <li>increase the capacity of families to develop relationships with fellow families, service
            providers, and others, and expand their knowledge of deaf-blindness and skills in 
            self-advocacy and self-empowerment.</li>
        </ul>

        <p>For more information about NCDB, visit <a href="http://www.nationaldb.org" title="NCDB Home">nationaldb.org</a> or contact:</p>
        <p class="mLeft12 mBottom24">
            NCDB<br />
            D. Jay Gense, Director<br />
            Teaching Research Institute<br />
            Western Oregon University<br />
            345 N. Monmouth Ave.<br />
            Monmouth, OR 97361<br />
            Voice: 800-438-9376<br />
            TTY: 800-854-7013<br />
            Fax: 503-838-8150<br />
            E-mail: <a href="mailto:info@nationaldb.org">info@nationaldb.org</a>
        </p>    
        <p>For more information about intervener services, see the <a href="http://nationaldb.org/ISSelectedTopics.php" title="Selected Topics" target="_blank">Selected Topics</a> page 
            <a href="http://nationaldb.org/ISSelectedTopics.php?topicCatID=10" target="_blank">Intervener Services</a>, or
            <a href="http://nationaldb.org/contact" title="Contact NCDB" target="_blank">contact NCDB</a>.
        </p>
        
        <div style="border-top: 1px dotted #999; margin-top: 12px; padding-top: 9px;">
            <p style="float: left;">
                <img src="images/footer_logos.gif" />
            </p>
            <div style="clear: left;"></div>
        </div>
    </div>    
    
</div>
<? include('includes/footer.php'); ?>
