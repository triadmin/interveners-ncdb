<? 
$pageTitle = 'Overview and Welcome';
include('includes/header.php'); ?>
<div role="main" class="clearfix" id="broadGoals">
    <h1 style="width: 100%; text-align: center; margin: 12px 0 12px 0;">Overview and Welcome</h1>
    
    <div class="clearfix"></div>
    <div id="content" class="plain">
        <h2>BACKGROUND</h2>
        <p class="mBottom24">The U.S. Department of Education’s Office of Special Education Programs 
            (OSEP) recognizes the current challenges faced by states and schools relative to the 
            provision of high-quality intervener services for children who are deaf-blind. To respond 
            to these challenges, OSEP asked the National Consortium on Deaf-Blindness (NCDB) to 
            conduct an initiative to 1) collect information about current intervener services across 
            the country and 2) develop recommendations for improving national, state, and local 
            intervener services based on an analysis of the information collected. The recommendations 
            presented on this website are NCDB’s response to OSEP’s request. They are intended to 
            promote positive developmental and educational outcomes for children who are deaf-blind, 
            from birth through age 21, by improving both the availability and quality of intervener 
            services throughout the United States.
        </p>

        <h2>WHAT ARE INTERVENER SERVICES?</h2>
        <p>The concept of intervener services for individuals who are deaf-blind arose in Canada in the
        1970s (McInnes, 1999, p. 75) and has been developing as a practice in the U. S. over the past
        several decades. Intervener services are provided by an intervener, typically a paraeducator, who:</p>
        
        <ul>
            <li style="list-style: none;">a) has received specialized, in-depth training in deaf-blindness and</li>
            <li style="list-style: none;">b) works one-to-one with an infant, child, or youth who is deaf-blind.</li>
            
        </ul>
        <p class="mBottom24">In school settings, the intervener serves as a member of the student’s educational team.</p>
        
        <p class="mBottom24">Deaf-blindness causes profound sensory deprivation. It creates a "disability of 
            access" to visual and auditory information about the environment (people, things, events) that 
            is necessary for learning, communication, and development (Alsop, et al., 2007, p. 1).  Without 
            frequent and responsive specialized support, a child with deaf-blindness has limited or no means 
            to predict events or communicate his or her needs. Without access to meaningful information that 
            sighted and hearing children receive incidentally, children with deaf-blindness are cut off from 
            essential formative learning experiences. Without a sense of safety and the ability to trust that 
            others will respond to their needs, their readiness to learn and achieve their potential is 
            compromised.</p>
        
        <p class="mBottom24">A skilled intervener can facilitate a child’s access to environmental information, support the
        development and use of communication, and promote social and emotional well-being (Alsop,
        Blaha, & Kloos, 2000). Interveners provide access to sensory information that would otherwise
        be unavailable to children whose vision and hearing are severely limited or absent. They
        enable children to become aware of what is occurring around them, attach language and meaning to all
        experiences, minimize the effects of multisensory deprivation, and empower children to have
        control over their lives (Henderson & Killoran, 1995, p. 3).</p>
        
        <p class="mBottom24">In bringing increased attention to this service through the work of NCDB’s Intervener Initiative,
        it is also important to identify what intervener services are not. An intervener is neither a teacher
        nor an expert in deaf-blind education. The provision of intervener services is not a panacea for
        surmounting the challenges inherent in educating a child who is deaf-blind. Rather, intervener services are
        one of a range of critical individualized supports that may be needed for children who are deaf-blind. 
        Interveners work closely with other team members, and they need ongoing support from
        teachers of children who are deaf-blind and other experts in deaf-blindness.</p>
        
        <h2>CURRENT CHALLENGES</h2>
        <p class="mBottom24">There is broad agreement in the field of deaf-blindness that interveners provide 
            a valuable service option in both school and community settings, for many children and youth who 
            are deaf-blind. High-quality intervener services, provided by a well-trained intervener, are 
            often necessary to provide an education in the least restrictive environment. Furthermore, 
            intervener services play a critical role in providing children and youth with deaf-blindness 
            access to the general curriculum. Unfortunately, there is a widespread lack of awareness of 
            the role of interveners in many school districts and an insufficient number of trained interveners 
            able to provide this valuable service. Currently, nationwide, only a very small percentage of 
            children who are deaf-blind receive intervener services.</p>
        
        <p class="mBottom24">Additionally, the scope and quality of intervener services vary significantly 
            from state to state and from school district to school district. It is clear that children and 
            youth who are deaf-blind, and their families, would be better served if partner stakeholders—including 
            families, NCDB, state deaf-blind projects, universities, researchers, schools, and early intervention 
            programs—would systematically collaborate in a nationwide effort to address the insufficiency of 
            intervener services in most states. 
        </p>
        
        <h2>REPORT ORGANIZATION</h2>
        <p>This report is organized in a format that highlights the following four broad goals:</p>
        <ul>
            <li>Goal 1: Increased recognition of intervener services by educational personnel
            and within local and state written policies;</li>
            <li>Goal 2: Training and support to increase the availability of well-trained,
            competent interveners;</li>
            <li>Goal 3: Creating systemic awareness and change through support for families as partners; and</li>
            <li>Goal 4: Long-term sustainability of high-quality intervener services across the
            nation through the inclusion of intervener services in national special
            education policy.</li>
        </ul>
        
        <p class="mBottom24">Using the links to each of these four goals on the bottom of the home
        page, the reader will move into a deeper presentation and discussion of recommendations that
        support achievement of each of the four goals.</p>
        
        <p class="mBottom24">Embedded links guide readers to recommendations that support the achievement of each goal. In
        turn, each of the recommendations includes implementation strategies that articulate action steps
        that NCDB, working in collaboration with state deaf-blind projects and other critical partners
        and stakeholders (e.g., families, university faculty, interveners, teachers, other service providers, and administrators), 
        can carry out or facilitate to achieve the recommendation. In addition, the
        recommendations are associated with anticipated outcomes. Supporting data and clarifying
        information are included throughout the report. We encourage readers to take time to fully explore the site and 
        browse each of the supporting narratives to which “read more” options lead.
        </p>
        
        <h2>ACKNOWLEDGMENTS</h2>
        <p class="mBottom24">The work involved in developing these recommendations could not have been successfully
        implemented to date without the active support and involvement of hundreds of people across
        the country who generously gave their time and willingly shared their wisdom and experience.
        Please explore the <a href="developmentProcess.php">Development Process</a> and <a href="acknowledgements.php">Acknowledgment</a> links at the top of the report
        home page to learn more about the strategies implemented to collect the information and data that
        provided a base for these recommendations. We include our grateful acknowledgment of the
        partners with whom we engaged.</p>

        <p class="mBottom24">Lastly, these recommendations are presented with a well-deserved acknowledgment 
            and extension of sincere gratitude to the many individuals across the country who pioneered the 
            foundational infrastructures for intervener services that are currently in place. The 
            recommendations presented here are an evolutionary extension of work begun by others. They 
            are very much grounded on the work of passionate and successful leaders in the field of 
            deaf-blindness who, for many years, led the charge in advocating for high-quality intervener 
            services.</p>

        <h2>REFERENCES</h2>

        <p>Alsop, L., Blaha, R., & Kloos, E. (2000). <em>The intervener in early intervention and educational
        settings for children and youth with deafblindness.</em> Monmouth, OR: Western Oregon
        University, Teaching Research, National Technical Assistance Consortium for Children
        and Young Adults Who Are Deaf-Blind.<br /><br />
        
        Alsop, L., Robinson, C., Goehl, K., Lace, J., Belote, M., & Rodriguez-Gil, G. (2007). 
        <em>Interveners in the classroom: Guidelines for teams working with students who are deafblind</em>.
        Logan, UT: SKI-HI Institute, Utah State University.<br /><br />
        
        Henderson, P., & Killoran, J. (1995). Utah enhances services for children who are deaf-blind.
        <em>Deaf-Blind Perspectives</em>, 3(1), 3–6.<br /><br />
        
        McInnes, J. M. (1999). Intervention. In J. M. McInnes (Ed.), <em>A guide to planning and support for
        individuals who are deafblind</em>, pp. 75–118. Toronto: University of Toronto Press.</p>
        
        <hr />

        <p>Suggested citation for this report: National Consortium on Deaf-Blindness. (2012).
            <em>Recommendations for improving intervener services</em>. Retrieved from 
        http://interveners.nationaldb.org.
        </p>
    </div>    
    
</div>
<? include('includes/footer.php'); ?>
