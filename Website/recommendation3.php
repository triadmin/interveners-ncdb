<? 
$page = 'goal2';
$pageTitle = 'Training: Recommendation 3';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="green clearfix">
      <div id="content" class="green">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Develop a national open-access intervener-training curriculum 
                  that aligns with the Council for Exceptional Children’s <em>Specialization 
                  Knowledge and Skill Set for Paraeducators Who Are Interveners for Individuals 
                  with Deaf-blindness</em>.</div>
              <div class="number green">3</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">Currently, there are two university-based online 
                  intervener training programs. In addition, six state deaf-blind 
                  projects report operating a training program in their states. They 
                  vary greatly in terms of format and intensity. In addition, although 
                  most state projects do not have formal intervener training programs, 
                  most do provide technical assistance to paraeducators as members of 
                  teams working with children who are deaf-blind. However, only small 
                  numbers of interveners have been trained in the majority of states. 
                  State deaf-blind projects report that insufficient funding and personnel 
                  limit the ability to create training materials, and most report that 
                  a standardized intervener training curriculum would help them better 
                  meet their state’s need for interveners. A national open-access 
                  curriculum, created by leading experts in the field of deaf-blindness, 
                  would support consistent training across the country. It could be used 
                  to begin or update intervener training programs and expand continuing 
                  education opportunities. A national curriculum would not only save 
                  state deaf-blind projects time and money, but also provide state 
                  educational systems with a resource that could be used to implement 
                  intervener preparation programs at local colleges and universities.</div>
              <a href="#" class="more green expando">Read more</a>
              
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix strategies">
                  <p>
                      Click "Read More" to see the 4 implementation strategies for this recommendation.
                  </p>
                  <p class="bullet">Establish a workgroup of individuals with expertise in 
                      intervener training to collaborate with NCDB on the development of 
                      an intervener training curriculum.</p>
                  <p class="bullet">Invite professionals from the field of deaf-blindness to submit intervener or general deaf-blind education training materials for review by the workgroup and possible incorporation into the curriculum.</p>
                  <p class="bullet">Develop the curriculum using new and existing materials.</p>
                  <p class="bullet">Create a web-based platform to host and provide free 
                      access to the curriculum.</p>
              </div>
              <a href="#" class="more green expando strategies">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>The Curriculum Should</h2>
              <div class="content closed clearfix">
                  <ul>
                      <li>Include content that can be used for initial training as well as for continuing education.</li>
                      <li>Offer materials in a variety of formats, including video footage.</li>
                      <li>Include modules that describe how to provide internships or practicum experiences for interveners-in-training.</li>
                      <li>Include guidelines for coaching and mentoring interveners.</li>
                      <li>Include guidelines that describe the level of resources and expertise needed to effectively implement the curriculum.</li>
                      <li>Support the use of the curriculum materials to provide training to 
                          family members and to teachers and other service providers (in addition to interveners) who work with children who are deaf-blind.</li>
                  </ul>    

              </div>
              <a href="#" class="more green expando">Read more</a>
          </div>
          
          <div class="item clearfix last">
              <h2>Anticipated Outcomes</h2>
              <div class="content">
                  <ul>
                      <li>Sufficient intervener preparation and training capacity across the country to meet a growing need for intervener services.</li>
                      <li>An increase in the number of well-trained interveners.</li>
                      <li>Increased consistency in the type of intervener training provided across the country.</li>
                  </ul> 
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="green">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec3.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
          <div class="panel">
              <a href="media/charts/Recommendation_3a1.png" title="" class="lightbox">
              <img src="images/chartThumb.png" title="Data charts" />
              </a>
              <a href="media/charts/Recommendation_3a2.png" title="" class="lightbox hide"></a>
              <a href="media/charts/Recommendation_3b.png" title="" class="lightbox hide"></a>
          </div>
          <div class="panel">
              <a href="http://player.vimeo.com/video/43133693" class="homeVideo" title="Video: Matt's Day Book">
                  <img src="images/videoStillSmall2.png" alt="Video: Matt's Day Book" />
              </a>
          </div>
          <div class="panel">
              <a href="http://c324175.r75.cf1.rackcdn.com/products/current%20training%20programs.pdf" title="Data Summary" target="_blank">
              <img src="images/dataSummaryThumb.png" title="Data Summary" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="broadGoalsRecognition.php" class="navButton blue left"><span>Recognition</span></a>
      <a href="recommendation4.php" class="navButton green right"><span>Recommendation 4&nbsp;</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>