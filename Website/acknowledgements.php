<? 
$pageTitle = 'Acknowledgments';
include('includes/header.php'); ?>
<div role="main" class="clearfix" id="broadGoals">
    <h1 style="width: 100%;">Acknowledgments</h1>
    <div class="clearfix"></div>
    <div id="content" class="plain">  
        <p class="mBottom24">Although NCDB determined the recommendations, we were assisted in this initiative
        by experts who have been actively engaged in intervener services activities for many years, as well as
        by individuals and agencies involved in the lives of children who are deaf-blind in other ways.
        They provided essential information for the development of the recommendations. The high
        level of interest and enthusiasm shown for this project demonstrates the importance of intervener
        services for children who are deaf-blind and their families.</p>

        <p class="mBottom24">We are deeply grateful to everyone who assisted us in this endeavor. We want to particularly
        acknowledge the following groups and individuals. Although all provided input and assistance,
        the recommendations are NCDB’s and do not necessarily reflect the opinions of the following
        participants or their agencies and organizations.</p>

        <h2>Consultants</h2>
        <p class="mBottom24">We would like to express our heartfelt thanks to Linda Alsop and Robbie Blaha who served
        as consultants to NCDB’s Intervener Services Initiative Team during our information-gathering phase.
        Ms. Alsop is a national leader on interveners and intervener services, having led the National
        Intervener Task Force since 2002, and served both as the director of the online Intervener Training Program at
        Utah State University and as the editor of a 2-volume textbook for interveners, <em>Understanding
        Deafblindness: Issues, Perspectives, and Strategies</em>. Robbie Blaha, also a national leader, works
        with the Texas Deafblind Project. Ms. Blaha has worked in the field of deaf-blind education for
        40 years and has been deeply involved in intervener services activities in Texas since the early
        1990s. We are grateful to both of them for sharing their extensive knowledge and wisdom with
        our Intervener Services Initiative Team.</p>
        
        <h2>State Deaf-Blind Projects</h2>
        <p class="mBottom24">We are also grateful to the state deaf-blind projects, our partners in the deaf-blind technical
        assistance network. Five of the state projects— Arizona, California, Minnesota, Texas, and
        Utah—have been working on activities related to intervener services since the 1990s, while
        others started this work more recently. Many representatives from the state projects have been involved
        in the National Intervener Task Force. Regardless of where along the continuum states are with
        respect to intervener services, their response to our requests for information was 
        overwhelmingly generous and extremely useful for this endeavor.</p>

        <p class="mBottom24">Of the 52 state deaf-blind projects, representatives from 42 (80%) responded to a lengthy, 
            in-depth anonymous survey about intervener services in their states and provided their professional
        opinions about various aspects of intervener services. An additional 16 project personnel
        responded to just the professional opinion part of the survey. Many projects also responded
        promptly to requests for additional information and disseminated announcements about NCDB’s
        surveys to families, interveners, and administrators.</p>

        <p class="mBottom24">Twenty-five of the fifty-two state projects participated in formal interviews about details of their project’s
        intervener services activities, and many provided documents used in those activities. Others
        participated in numerous informal conversations with NCDB staff. A special thank you goes to the
        Minnesota and Texas deaf-blind projects whose staff members gave generously of their time and 
        knowledge when they provided onsite overviews of their activities to NCDB staff.</p>
        
        <h2>Families</h2>
        <p class="mBottom24">Families provided extensive details about their personal experiences with intervener services
        as well as their insights into the overall system of intervener services via the parent/guardian
        survey and during the family panel discussion. This information was extremely helpful during
        the development of the recommendations. We want to thank the parents who provided advice
        to NCDB during the development of the parent/guardian survey—Laura Fonseca, Diane Foster,
        Melanie Knapp, Djenne and Michael Morris, and Sally Prouty—as well as the 119 anonymous
        individuals who took the survey.</p>

        <h2>University Faculty</h2>
        <p class="mBottom24">Linda Alsop, director of the intervener training program at Utah State University, 
            and Alana Zambone, director of the intervener training program at East Carolina University, both 
            participated in extensive interviews about their programs and shared their knowledge of intervener 
            services in the U.S. and their states (intervener services initiatives have been ongoing in Utah 
            and North Carolina for many years). They also provided very helpful documentation. Ms. Alsop and 
            Dr. Zambone participated on panels along with other university faculty.</p>

        <h2>Interveners</h2>
        <p class="mBottom24">Input from interveners was essential for us to learn about the level of support 
            that is available to interveners working in classrooms and other settings and to gather 
            preliminary data about the current workforce of interveners (e.g., years of education, type 
            of intervener training). Many thanks to Tammi Morgan, Vicki Spencer, and Erin Hladky Yanez, 
            who provided advice during the development of the NCDB survey for interveners, and to the 
            128 anonymous individuals who completed that survey.</p>

        <h2>Administrators</h2>
        <p class="mBottom24">A range of administrators employed in schools and local and state early intervention and
        education agencies shared their wisdom and experience via an anonymous survey and on our
        administrators’ panel. They provided insight into intervener services in their states and their
        own knowledge of and attitudes toward intervener services. Administrators are on the front
        lines when it comes to providing intervener services for children who are deaf-blind. Their knowledge of the 
        educational system and the place within that system where intervener services fit was invaluable.</p>
        
        <h2>Panel Members</h2>
        <p class="mBottom24">Input via panel discussions from individuals with knowledge of intervener services was 
            an important part of NCDB’s recommendations development process. The information gained from these 
            discussions was illuminating, and the panelists’ comments were thoughtful and perceptive. Both types 
            of information provided context and depth to the data gathered during Phase 1 of the process. We want 
            to thank all of the following individuals for the time and energy they committed to this endeavor.</p>
        
        <table>
            <tr>
                <td style="width: 33%;"><strong>Panel 1: State Deaf-Blind Project<br />Personnel &amp; University Faculty</strong></td>
                <td style="width: 33%;"><strong>Panel 2: Families</strong></td>
                <td style="width: 33%;"><strong>Panel 3: Interveners</strong></td>
            </tr>
            <tr>
                <td>
                    Karen Blankenship<br />
                    Leslie Buchanan<br />
                    Megan Cote<br />
                    Diane Kelly<br />
                    Beth Kennedy<br />
                    Cheryl Levasseur<br />
                    Jennifer Miller<br />
                    Chris Montgomery<br />
                    Sam Morgan<br />
                    Kristen Parsons<br />
                    Sally Prouty<br />
                    Alana Zambone
                </td>
                <td>
                    Stephanie Bernhagen<br />
                    Ellen Bowman<br />
                    Robbie Caldwell<br />
                    Alison Caputo<br />
                    Deanne Curran<br />
                    Diane Foster<br />
                    Vivecca Hartman<br />
                    Melanie Knapp<br />
                    Cyndie Pfohler<br />
                    Lisa Rohr<br />
                    Deanna Rothbauer
                </td>
                <td>
                    Brenda Bujold<br />
                    Amy Harrison<br />
                    Jennifer James<br />
                    Nancy Kotyk<br />
                    Barbara Martin<br />
                    Tammi Morgan<br />
                    Cindy Skiles<br />
                    Vicki Spencer<br />
                    Erin Hladky Yanez
                </td>
            </tr>
            <tr>
                <td style="padding-top: 12px;"><strong>Panel 4: State Deaf-Blind Project<br />Personnel &amp; University Faculty</strong></td>
                <td style="padding-top: 12px;"><strong>Panel 5: Teachers</strong></td>
                <td style="padding-top: 12px;"><strong>Panel 6: Administrators</strong></td>
            </tr>
            <tr>
                <td>
                    Linda Alsop<br />
                    Susan Bashinski<br />
                    Maurice Belote<br />
                    Mark Campano<br />
                    Cathy Lyle<br />
                    Cyral Miller<br />
                    Rose Moehring<br />
                    Terry Rafalowski-Welch<br />
                    Cindi Robinson<br />
                    Eva Scott<br />
                    David Wiley
                </td>
                <td>
                    Doreen Bohm<br />
                    Tina Hertzog<br />
                    Kristin Knight<br />
                    Jennifer Lester<br />
                    Angel Perez<br />
                    Sarah Shreckhise<br />
                    Becky Wolf
                </td>
                <td>
                    Jackie Brennan<br />
                    Maggie Mathews<br />
                    Brent Pitt<br />
                    Gayle Robbins<br />
                    Elaine Robertson<br />
                    William Trant<br />
                    Steve Young
                </td>
            </tr>
        </table>
        
        
    </div>
</div>    
<? include('includes/footer.php'); ?>
