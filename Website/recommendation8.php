<? 
$page = 'goal3';
$pageTitle = 'Families: Recommendation 8';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="maroon clearfix">
      <div id="content" class="maroon">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Develop information resources and tools and disseminate them to family 
                  members to increase their knowledge of intervener services and enhance their ability to 
                  communicate effectively with educators, administrators, and others about those services.</div>
              <div class="number maroon">8</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">
                  <p style="margin-bottom: 18px;">Although parents who participated in the family panel were very knowledgeable about intervener services, many said that it had been difficult for them to obtain that knowledge and wished they had learned about interveners sooner. They expressed concern that many families across the country remain uninformed, and they believe families need better access to information in order to participate as full partners on IFSP/IEP teams making decisions about intervener services for their children.</p>
                  
                  <p>A number of state deaf-blind projects and other organizations have already developed family-focused products about intervener services, so an important component of the strategies for this recommendation is to highlight what already exists and develop additional information resources as needed. In particular, active collaboration between NCDB, state deaf-blind projects, the National Family Association for Deaf-Blind (NFADB), the National Deafblind Intervener Initiative (NDBII) Parent Group, and parent training and information centers (PTIs) will be essential.</p>
               </div>
              <a href="#" class="more maroon expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix strategies">
                  <p>
                      Click "Read More" to see the 3 implementation strategies for this recommendation.
                  </p>
                  <p class="bullet">Review existing family-focused resources related to intervener 
                      services.</p>
                  <p class="bullet">Use existing resources (if available) or develop new products 
                      that families can use to:</p>
                  <ul class="ulBumpLeft">
                      <li>promote communication about intervener services with early interventionists, educators, and administrators,</li>
                      <li>inform decisions related to intervener services for their child, and</li>
                      <li>inform state and local policies to encourage and promote high-quality intervener services.</li>
                  </ul>
                  <p class="bullet">Collaborate with family organizations to distribute information to families 
                      who have limited knowledge of interveners.  This will include efforts to 
                      reach out to groups who are typically underrepresented (e.g., racial and 
                      ethnic minorities, families who live in rural areas, and families who are 
                      socioeconomically disadvantaged).</p>
              </div>
              <a href="#" class="more maroon expando strategies">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Anticipated Outcomes</h2>
              <div class="content clearfix">
                  <ul>
                      <li>An increase in the number of family members who are knowledgeable about 
                          intervener services.</li>
                      <li>An increase in the number of family members who can effectively 
                          advocate for intervener services for their children when appropriate.</li>
                      <li>An increase in the number of family members who participate in initiatives 
                          to improve intervener services.</li>

                  </ul>
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="maroon">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec8.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
          <div class="panel">
              <a href="media/charts/Recommendation_8a.png" title="" class="lightbox">
              <img src="images/chartThumb.png" title="Data charts" />
              </a>
              <a href="media/charts/Recommendation_8b.png" title="" class="lightbox hide"></a>
          </div>
          <div class="panel">
              <a href="http://c324175.r75.cf1.rackcdn.com/products/family%20experiences.pdf" title="Data Summary" target="_blank">
              <img src="images/dataSummaryThumb.png" title="Data Summary" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="broadGoalsTraining.php" class="navButton green left"><span>Training</span></a>
      <a href="recommendation9.php" class="navButton maroon right"><span>Recommendation 9&nbsp;</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>