<? 
$page = 'goal1';
$pageTitle = 'Recognition: Recommendations';
include('includes/header.php') ;?>
    <div role="main" id="broadGoals" class="clearfix">
      <h1>Goal 1</h1>
      <p class="header blue">Increase recognition and appropriate use of intervener 
                  services for children and youth who are deaf-blind.</p>
      <div class="clearfix"></div>
      
      <div id="content" class="blue">
        <div class="subheader closed">
            <h2>Discussion</h2>
            <div class="content">
                <p>Over the past two decades, significant efforts by many individuals across the nation have improved the availability of intervener services for children who are deaf-blind.  Families have educated policymakers about the role of interveners and the positive impact an intervener can have on the education of a child who is deaf-blind.  A number of state deaf-blind projects have developed creative ways to train interveners and support them in classrooms.  Since 2002, there has been a national intervener task force that has developed valuable resources, including materials that raise awareness of intervener services and guidelines for intervener competencies.  Nationally, two universities now offer online intervener training.</p>

                <p>Unfortunately, despite these strides forward, the national infrastructure to support 
                    intervener services is fragmented and unevenly distributed across the country and 
                    within states.  Only a few states officially recognize intervener services as a related 
                    service option or mention them in state special education administrative rules.  
                    While it is true that some children for whom an educational team determines that 
                    intervener services are needed to ensure a free, appropriate public education (FAPE) 
                    do receive them, many who might also require them do not.</p>

                <p>Educational teams (IEP teams and IFSP teams) are often unaware of the purpose of intervener services and lack access to resources that would help them make good decisions about a child’s need for them.  These challenges, as well as the difficulty that school districts often face finding well-trained interveners (or individuals who could be trained), compromise the provision of intervener services for many children.</p>

                <p>The recommendations for this goal are intended to extend the progress made so 
                    far by families, state deaf-blind projects, university faculty, and others to 
                    increase the recognition and appropriate use of intervener services as 
                    evidenced by the following:

                <ul>
                    <li>personnel responsible for the education of children who are deaf-blind who 
                        clearly understand the purpose of intervener services;</li>
                    <li>families who clearly understand the purpose of intervener services; and</li>
                    <li>national, state, and local education policies and practices that reflect and 
                        support the provision of intervener services for children who are deaf-blind 
                        if an IEP or IFSP team determines they are needed.</li>
                </ul>
            </p>

                <p>The recommendations for Goal 1 provide strategies to a) coordinate efforts to 
                    improve our nation’s understanding and use of intervener services and b) 
                    establish intervener services as a universally understood related service or 
                    early intervention option for children who are deaf-blind.  Successful 
                    implementation of the strategies will require in-depth participation from many 
                    stakeholders including:
                <ul>
                    <li>the National Consortium on Deaf-Blindness (NCDB),</li>
                    <li>families,</li>
                    <li>state deaf-blind projects,</li>
                    <li>university faculty,</li>
                    <li>interveners,</li>
                    <li>early intervention and educational administrators,</li>
                    <li>teachers and other service providers, and</li>
                    <li>researchers in low-incidence disabilities.</li>
                </ul>
                </p>

                <p>Working together to solve problems is not new to this community of individuals who are 
                    involved in the lives of children who are deaf-blind.  There are already informal and 
                    formal collaborations established across agencies, organizations, individuals, and 
                    families. These partnerships provide a strong foundation that supports implementation 
                    of the identified strategies through a new centralized system for national coordination 
                    of activities to improve intervener services.</p>
            </div>          
        </div>
        <a href="#" class="more blue expando">Read more</a>   
        <div class="clearfix"></div>
        
        <div id="broadGoalsContainer">
            
            <!-- BEGIN :: Broad Goal Panel -->
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 1</div>
                    <div class="photo blue"></div>
                    <div class="text blue">Develop a coordinated and expanded national approach to provide state 
                        and local early intervention and education agencies with information and tools needed 
                        to understand and use intervener services.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation1.php" class="button blue">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END :: Broad Goal Panel -->
            
            <div class="broadGoalPanel">
                <div class="content">
                    <div class="header">Recommendation 2</div>
                    <div class="photo blue"></div>
                    <div class="text blue">Coordinate and expand efforts to inform national, state, 
                        and local policies and practices so that they reflect and support the provision of 
                        intervener services for a child or youth who is deaf-blind when needed.</div>
                    <div class="footer">
                        <div class="buttonContainer">
                            <a href="recommendation2.php" class="button blue">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Put more broad goals here -->
            <div class="clearfix"></div>
        </div>     
        <div class="clearfix"></div>
      </div>
  </div>
  <? include('includes/footer.php') ;?>