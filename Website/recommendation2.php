<? 
$page = 'goal1';
$pageTitle = 'Recognition: Recommendation 2';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="blue clearfix">
      <div id="content" class="blue">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Coordinate and expand efforts to inform national, state, 
                  and local policies and practices so that they reflect and support the provision of 
                  intervener services for a child or youth who is deaf-blind when needed.</div>
              <div class="number blue">2</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">Deaf-blindness is, and will likely continue to be, the lowest of 
                  all low-incidence disabilities. In addition, the impact of this disability on development 
                  and learning is unique. Gaining and maintaining attention for the highly individualized 
                  services needed by such a low-incidence and diverse group is challenging. It is critical, 
                  therefore, that national, state, and local policies and practices appropriately reflect 
                  the unique needs of children who are deaf-blind, including the provision of intervener 
                  services when necessary. Without these services many children may not have access to a 
                  free and appropriate public education (FAPE).</div>
              <a href="#" class="more blue expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix strategies">
                  <p>
                      Click "Read More" to see the 6 implementation strategies for this recommendation.
                  </p>
                  <p class="bullet">Produce and disseminate guidelines that IFSP/IEP teams can use 
                      to make informed decisions about the need for initial or continued use of 
                      intervener services for an individual child or youth.</p>
                  <p class="bullet">Using the core products described in Recommendation 1:</p>
                  <ul class="ulBumpLeft">
                      <li>promote best practices for intervener services via information dissemination and technical assistance activities; and,</li>
                      <li>systematically disseminate resources to lawmakers, other policymakers, and union 
                          representatives to inform policies related to intervener services.</li>
                  </ul>
                  <p class="bullet">Work with OSEP to encourage U.S. Department of Education cross-agency (e.g., OSEP, Rehabilitation Services Administration, National Institute on Disability and Rehabilitation Research) recognition of intervener services.</p>
                  <p class="bullet">Work with state and national special education organizations and centers (e.g., Regional Resource Centers, the National Association of State Directors of Special Education, Parent Training and Information Centers) to design and implement strategies that inform policies and practices related to intervener services.</p>
                  <p class="bullet">Work with state special education advisory councils to raise individual states’ awareness of intervener services.</p>
                  <p class="bullet">Contribute to the growth of knowledge related to intervener services in the following ways:</p>
                  <ul class="ulBumpLeft">
                      <li>develop professional publications including technical reports or peer-reviewed 
                          journal articles that summarize available data about interveners and describe the 
                          history and current status of intervener services in the U.S., and</li>
                      <li>promote research on intervener services by:
                          <ul>
                              <li>facilitating discussions among graduate students and researchers within 
                                  the field of deaf-blindness,</li>
                              <li>assisting researchers in identifying children and families who can 
                                  participate in research studies,</li>
                              <li>providing library support (e.g., literature searching) to researchers 
                                  working in this area, and</li>
                              <li>identifying possible funding sources for intervener services research.</li>
                          </ul>
                      </li>
                  </ul>     
              </div>
              <a href="#" class="more blue expando strategies">Read more</a>
          </div>
          
          <div class="item clearfix last">
              <h2>Anticipated Outcomes</h2>
              <div class="content">
                  <ul>
                      <li>Improved access to FAPE for children who are deaf-blind.</li>
                      <li>An increase in the number of children for whom the IFSP/IEP appropriately reflects intervener services.</li>
                      <li>Improved achievement of IFSP/IEP goals and objectives for children who are deaf-blind.</li>
                      <li>Local, state, and national policies and practices that reflect the need for intervener
                        services for children who are deaf-blind.</li>
                      <li>Increased visibility of intervener services in the professional special education literature.</li>
                  </ul>
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="blue">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec2.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
          <div class="panel">
              <a href="http://player.vimeo.com/video/43216800" class="homeVideo" title="Video: Johanna Borg">
                  <img src="images/videoStillSmall3.png" alt="Video: Johanna Borg" />
              </a>
          </div>
          <div class="panel">
              <a href="http://www.nationaldb.org/documents/products/recognition.pdf" title="Data Summary" target="_blank">
              <img src="images/dataSummaryThumb.png" title="Data Summary" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="recommendation1.php" class="navButton blue left"><span>Recommendation 1</span></a>
      <a href="broadGoalsTraining.php" class="navButton green right"><span>Training&nbsp;</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>