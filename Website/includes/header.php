<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title>
      <?
      if (!empty($pageTitle)) {
          echo $pageTitle;
      } else {
          echo 'Intervener Services Recommendations';
      }        
      ?>
      </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">

  <link rel="icon" href="favicon.ico" type="image/ico" />
  <link rel="shortcut icon" href="favicon.ico" type="image/ico" />   
  <link rel="stylesheet" href="css/style.css">
  <!--[if IE]>
  <link rel="stylesheet" href="css/styleIE.css">
  <![endif]-->
  <link rel="stylesheet" href="css/colorbox.css">
  <link rel="stylesheet" href="css/flexslider.css">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,300,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300' rel='stylesheet' type='text/css'>
  <!--<link href='http://fonts.googleapis.com/css?family=Nobile:400,400italic,700italic,700' rel='stylesheet' type='text/css'>-->
  
  <script src="js/plugins/modernizr-2.5.3.min.js"></script>
</head>
<body>
    <header>
        <div id="container">
            <a href="http://interveners.nationaldb.org" id="logo" title="NCDB Intervener Recommendations 2012"><h1>NCDB Intervener Recommendations 2012</h1></a>
            <div id="menuContainer">
                <div id="topMenuContainer">
                    <a href="about.php" title="About NCDB">About NCDB</a>&nbsp;&nbsp;|&nbsp;
                    <a href="developmentProcess.php" title="Development Process">Development Process</a>&nbsp;&nbsp;|&nbsp;
                    <a href="acknowledgements.php" title="Acknowledgments">Acknowledgments</a>&nbsp;&nbsp;|&nbsp;
                    <a href="documents/NCDB-Intervener-Services-Recommendations.pdf" title="Print Version">Print Version</a>&nbsp;&nbsp;|&nbsp;
                    <a href="http://www.nationaldb.org" title="NCDB Home">NCDB Home</a>
                    
                    <div style="float: right; margin-left: 12px;">
                      <div id="embed_launch_pad"> </div>
                      <script type="text/javascript" src="https://nationaldb.org/assets/js/widgets/widget_launchpad.js" 
                      data-site="intervener-recommendations" 
                      data-align="bottom-left"></script>
                    </div>
                </div>
                <div id="mainMenuContainer">
                    <ul>
                        <li
                            <? if ($page == 'home') { echo 'class="ltGray selected"'; } else { echo 'class="ltGray"'; } ?>
                        ><a href="index.php" title="Home page">Home</a></li>
                        <li
                            <? if ($page == 'goal1') { echo 'class="blue selected"'; } else { echo 'class="blue"'; } ?>
                        ><a href="broadGoalsRecognition.php">Goal 1: Recognition</a></li>
                        <li
                            <? if ($page == 'goal2') { echo 'class="green selected"'; } else { echo 'class="green"'; } ?>
                        ><a href="broadGoalsTraining.php">Goal 2: Training</a></li>
                        <li
                            <? if ($page == 'goal3') { echo 'class="maroon selected"'; } else { echo 'class="maroon"'; } ?>
                        ><a href="broadGoalsFamilies.php">Goal 3: Families</a></li>
                        <li class="last
                            <? if ($page == 'goal4') { echo ' orange selected'; } else { echo ' orange'; } ?>
                        "><a href="broadGoalsSustainability.php">Goal 4: Sustainability</a></li>
                    </ul>
                </div>    
            </div>
            <div class="clearfix"></div>
        </div>  
    </header>
