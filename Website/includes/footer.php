<footer>
      <div id="content">
          <a href="http://www.nationaldb.org" title="NCDB Home">NCDB: The Teaching Research Institute</a> | <a href="http://www.wou.edu" title="Western Oregon University">Western Oregon University</a>
          <p class="disclaimer">The contents of these recommendations were developed under a grant from the US Department of Education, #H326T130013. However, those contents do not necessarily represent the policy of the US Department of Education, and you should not assume endorsement by the Federal Government.  Project Officer, Jo Ann McCann, Office of Special Education Programs.</p>
      </div>
</footer>


  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script src="js/siteGlobal.js"></script>
  <script src="js/plugins/jquery.colorbox.js"></script>
  <script src="js/plugins/jquery.flexslider-min.js"></script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1427386-8']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>
