<? include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="blue clearfix">
      <h1>Recognition</h1>
      <p class="header blue">Promote an increased understanding of intervener services at local, state, and
      national levels, and enhance systemic recognition of and need for quality intervener services
      for children who are deaf-blind.</p>
      <div class="clearfix"></div>
      
      <div id="content" class="blue">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                  Sed mollis felis eget tellus convallis et varius nunc fermentum. Donec 
                  id consequat risus. Phasellus sit amet enim tellus. Vivamus vitae condimentum 
                  eros. Nullam dapibus tempor imperdiet. Pellentesque ut aliquet nisl. Praesent 
                  in turpis ac quam vehicula vulputate.</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why is this important?</h2>
              <div class="content closed">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                  Sed mollis felis eget tellus convallis et varius nunc fermentum. Donec 
                  id consequat risus. Phasellus sit amet enim tellus. Vivamus vitae condimentum 
                  eros. Nullam dapibus tempor imperdiet. Pellentesque ut aliquet nisl. Praesent 
                  in turpis ac quam vehicula vulputate.</div>
              <a href="#" class="more blue expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                  Sed mollis felis eget tellus convallis et varius nunc fermentum. Donec 
                  id consequat risus. Phasellus sit amet enim tellus. Vivamus vitae condimentum 
                  eros. Nullam dapibus tempor imperdiet. Pellentesque ut aliquet nisl. Praesent 
                  in turpis ac quam vehicula vulputate.</div>
              <a href="#" class="more blue expando">Read more</a>
          </div>
          
          <div class="item clearfix last">
              <h2>Anticipated Outcomes</h2>
              <div class="content closed">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                  Sed mollis felis eget tellus convallis et varius nunc fermentum. Donec 
                  id consequat risus. Phasellus sit amet enim tellus. Vivamus vitae condimentum 
                  eros. Nullam dapibus tempor imperdiet. Pellentesque ut aliquet nisl. Praesent 
                  in turpis ac quam vehicula vulputate.</div>
              <a href="#" class="more blue expando">Read more</a>
          </div>
          
      </div>
      <div id="sideBar" class="blue">
          <div class="panel">
              
          </div>
          <div class="panel"></div>
          <div class="panel"></div>
      </div>
  </div>
  <? include('includes/footer.php'); ?>