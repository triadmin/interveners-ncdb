<? 
$page = 'goal3';
$pageTitle = 'Families: Recommendation 9';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="maroon clearfix">
      <div id="content" class="maroon">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Develop and implement strategies that create opportunities for 
                  families to share ideas and experiences and work together to address intervener 
                  services at local, state, and national levels.</div>
              <div class="number maroon">9</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed">
                  <p>Comments that NCDB received in response to a survey for parents and guardians and via 
                      input during a family panel discussion illustrate how important it is for families 
                      to have opportunities to share their experiences related to intervener services, and 
                      especially to have opportunities to talk to each other about those experiences.  
                      Because deaf-blindness is a rare disability, families of children who are deaf-blind 
                      are typically separated from each other by great distances.  Many state deaf-blind 
                      projects already conduct activities to help family members form connections within 
                      their states.  The strategies below are meant to augment those activities and bring 
                      increased focus to intervener services.  They also promote continued involvement by 
                      families in the effort to expand and improve intervener services in the United States.</p>
               </div>
              <a href="#" class="more maroon expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix strategies">
                  <p>
                      Click "Read More" to see the 3 implementation strategies for this recommendation.
                  </p>
                  <p class="bullet">Establish accessible web-based and/or telephone groups where 
                      family members of children who are deaf-blind can share ideas and experiences about intervener services.</p>
                  <p class="bullet">Partner with key family organizations (e.g., NFADB, NDBII Parent Group, PTIs) to implement strategies for recommendations that promote 
                      appropriate effective intervener services for children who are deaf-blind. </p>
                  <p class="bullet">Develop a curriculum module about intervener services to supplement current 
                      family leadership curricula that educate family members and help them to mentor others.</p>
              </div>
              <a href="#" class="more maroon expando strategies">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Anticipated Outcomes</h2>
              <div class="content clearfix">
                  <ul>
                      <li>Increased family understanding of intervener services.</li>
                      <li>Increased opportunities for family members to connect online or by telephone to discuss their experiences with intervener services.</li>
                      <li>Increased collaboration among families on activities to promote appropriate intervener services.</li>

                  </ul>
              </div>
          </div>
      </div>
      <div id="sideBar" class="maroon">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec9.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
          <div class="panel">
              <a href="media/charts/Recommendation_9a.png" title="" class="lightbox">
              <img src="images/chartThumb.png" title="Data charts" />
              </a>
          </div>
      </div>  
  </div>
  <div class="bottomNavBar">
      <a href="recommendation8.php" class="navButton maroon left"><span>Recommendation 8</span></a>
      <a href="broadGoalsSustainability.php" class="navButton orange right"><span>Sustainability&nbsp;</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>