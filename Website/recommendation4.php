<? 
$page = 'goal2';
$pageTitle = 'Training: Recommendation 4';
include('includes/header.php'); ?>
    <div role="main" id="recommendations" class="green clearfix">
      <div id="content" class="green">
          <div class="item recommendation">
              <h2>Recommendation</h2>
              <div class="content">Develop strategies to ensure that interveners have knowledgeable supervisors and access to experts in deaf-blindness who can provide consultation and coaching.</div>
              <div class="number green">4</div>
              <div class="clearfix"></div>
          </div>
          
          <div class="item clearfix">
              <h2>Why This Is Important</h2>
              <div class="content closed"><p style="margin-bottom: 18px;">Although comprehensive initial training provides interveners 
                  with basic skills and knowledge, it is important to understand that interveners are 
                  neither teachers nor experts in deaf-blind education. They need knowledgeable supervisors 
                  and ongoing support from experts, such as teachers of the deaf-blind, who can a) provide 
                  coaching and close supervision to interveners who are enrolled in a training program and 
                  early in their careers, b) help interveners acquire and maintain the knowledge and skills 
                  needed to work effectively with particular children, and c) provide ongoing mentorship 
                  and professional development. (Note: in this report we use the term “teacher of the 
                  deaf-blind” to refer to a teacher who has extensive knowledge and expertise in the 
                  education of children with deaf-blindness. However, the majority of states do not 
                  currently have a specific certification process for this specialization.)</p>
                  
                  <p style="margin-bottom: 18px;">Unfortunately, classroom teachers and other educational team members often lack 
                  expertise in deaf-blindness and the state projects, which are funded to 
                  increase statewide capacity to serve children who are deaf-blind, do not have 
                  sufficient staffing to provide ongoing consultation to a large number of 
                  educational teams. In addition, there is a severe shortage of teachers of 
                  the deaf-blind to serve on educational teams or as consultants to those teams. 
                  Building a system that supports quality supervision and access to experts knowledgeable 
                  about deaf-blindness will be difficult because of budgetary constraints in every state but 
                  critical for the successful implementation of intervener services.</p>
                  
                  <p>As noted in the introduction to this report, intervener services are just one of a 
                      range of individualized supports that may be required by children who are deaf-blind, 
                      including supports and services provided by other trained personnel. In particular, 
                      expansion of the workforce of trained teachers of the deaf-blind is needed. This 
                      recommendation considers the need for more teachers of the deaf-blind in the context 
                      of support for intervener services. It should be noted, however, that while beyond 
                      the scope of these recommendations, there is a critical need for a broader range of 
                      strategies to address the current shortage of these teachers.</p>
              </div>
              <a href="#" class="more green expando">Read more</a>
          </div>
          
          <div class="item clearfix">
              <h2>Implementation Strategies</h2>
              <div class="content closed clearfix strategies">
                  <p>
                      Click "Read More" to see the 5 implementation strategies for this recommendation.
                  </p>
                  <p class="bullet">Use the intervener training curriculum described in Recommendation 3 to train teachers and other team members about deaf-blindness and the role of the intervener.</p>
                  <p class="bullet">Identify successful models used by state deaf-blind projects, university programs, and school districts that provide on-the-job support to interveners.</p>
                  <p class="bullet">Replicate these models to support an increasing number of interveners.</p>
                  <p class="bullet">In partnership with a broad group of stakeholders, examine the causes of the shortage of local experts in deaf-blindness, including teachers of the deaf-blind, and identify strategies to alleviate the shortage. </p>
                  <p class="bullet">Design and implement strategies to provide distance consultation, coaching, and mentoring through the use of technology applications.</p>
              </div>
              <a href="#" class="more green expando strategies">Read more</a>
          </div>
          
          <div class="item clearfix last">
              <h2>Anticipated Outcomes</h2>
              <div class="content">
                  <ul>
                      <li>Improved supervision of interveners.</li>
                      <li>Increased opportunities for coaching and mentoring from teachers of the deaf-blind.</li>    
                  </ul>
              </div>
          </div>
          
      </div>
      <div id="sideBar" class="green">
          <div class="panel">
              <a href="media/community_voices/NCDB-Community_Voices_Rec4.png" title="Community Voices" class="lightbox">
              <img src="images/communityVoicesThumb.png" title="Community Voices" />
              </a>
          </div>
          <div class="panel">
              <a href="media/charts/Recommendation_4a.png" title="" class="lightbox">
              <img src="images/chartThumb.png" title="Data charts" />
              </a>
              <a href="media/charts/Recommendation_4b.png" title="" class="lightbox hide"></a>
          </div>
          <div class="panel">
              <a href="http://player.vimeo.com/video/43132503" title="Video: Intervener ~ From a Parent's Perspective" class="homeVideo">
                  <img src="images/videoStillSmall1.png" alt="Video: Intervener ~ From a Parent's Perspective" />
              </a>
          </div>
          <div class="panel">
              <a href="http://www.nationaldb.org/documents/products/intervener%20supports.pdf" title="Data Summary" target="_blank">
              <img src="images/dataSummaryThumb.png" title="Data Summary" />
              </a>
          </div>
      </div>
  </div>
  <div class="bottomNavBar">
      <a href="recommendation3.php" class="navButton green left"><span>Recommendation 3</span></a>
      <a href="recommendation5.php" class="navButton green right"><span>Recommendation 5&nbsp;</span></a>       
  </div>
  <div class="clearfix"></div>
  <? include('includes/footer.php'); ?>